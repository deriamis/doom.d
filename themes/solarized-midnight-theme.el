;;; solarized-midnight-theme.el --- an even darker variant of Solarized dark -*- lexical-binding: t; no-byte-compile: t; -*-
;;
;; Added:
;; Author: logc <https://github.com/deriamis>
;; Maintainer:
;; Source: https://meat.io/oksolar.json
;; Source: https://meat.io/oksolar
;;
;;; Commentary:
;;; Code:

(require 'doom-themes)


;;
;;; Variables

(defgroup solarized-midnight-theme nil
  "Options for the `solarized-midnight' theme."
  :group 'doom-themes)

(defcustom solarized-midnight-brighter-modeline nil
  "If non-nil, more vivid colors will be used to style the mode-line."
  :group 'solarized-midnight-theme
  :type 'boolean)

(defcustom solarized-midnight-brighter-comments nil
  "If non-nil, comments will be highlighted in more vivid colors."
  :group 'solarized-midnight-theme
  :type 'boolean)

(defcustom solarized-midnight-brighter-text nil
  "If non-nil, default text will be brighter."
  :group 'solarized-midnight-theme
  :type 'boolean)

(defcustom solarized-midnight-padded-modeline doom-themes-padded-modeline
  "If non-nil, adds a 4px padding to the mode-line.
Can be an integer to determine the exact padding."
  :group 'solarized-midnight-theme
  :type '(choice integer boolean))


;;
;;; Theme definition

(def-doom-theme solarized-midnight
  "An even darker theme inspired by Solarized Dark."

  ;; name        default   256       16
  ((bg         '("#001717" "#001717" "brightblack"))
   (fg         (if solarized-midnight-brighter-text
                   '("#9FB1B3" "#9FB1B3" "brightwhite")
                 '("#7E8082" "#7E8082" "brightwhite")))

   ;; These are off-color variants of bg/fg, used primarily for `solaire-mode',
   ;; but can also be useful as a basis for subtle highlights (e.g. for hl-line
   ;; or region), especially when paired with the `doom-darken', `doom-lighten',
   ;; and `doom-blend' helper functions.
   (bg-alt     '("#171717" "#171717" "brightblack"))
   (fg-alt     '("#586E75" "#586E75" "white"))

   ;; These should represent a spectrum from bg to fg, where base0 is a starker
   ;; bg and base8 is a starker fg. For example, if bg is light grey and fg is
   ;; dark grey, base0 should be white and base8 should be black.
   (base0      '("#171717" "#171717" "black"))
   (base1      '("#074642" "#074642" "brightblack"))
   ;; NOTE: base 2 never used, left as-is in Solarized
   (base2      '("#00212C" "#00212C" "brightblack"))
   (base3      '("#586E75" "#586E75" "brightblack"))
   (base4      '("#657B83" "#657B83" "brightblack"))
   (base5      '("#7E8082" "#7E8082" "brightblack"))
   (base6      '("#93A1A1" "#93A1A1" "brightblack"))
   (base7      '("#EEE8D5" "#EEE8D5" "brightblack"))
   (base8      '("#FDF6E3" "#FDF6E3" "white"))

   (grey       base4)
   (red        '("#DC322F" "#DC322F" "red"))
   (orange     '("#CB4B16" "#CB4B16" "brightred"))
   (green      '("#859900" "#859900" "green"))
   (teal       '("#586E75" "#586E75" "brightgreen"))
   (yellow     '("#AC8300" "#AC8300" "yellow"))
   (blue       '("#839496" "#839496" "brightblue"))
   (dark-blue  '("#2B90D8" "#2B90D8" "blue"))
   (magenta    '("#D33682" "#D33682" "magenta"))
   (violet     '("#6C71C4" "#6C71C4" "brightmagenta"))
   (cyan       '("#93A1A1" "#93A1A1" "brightcyan"))
   (dark-cyan  '("#2AA198" "#2AA198" "cyan"))

   ;; face categories -- required for all themes
   (highlight      blue)
   (vertical-bar   (doom-darken base1 0.5))
   (selection      dark-blue)
   (builtin        blue)
   (comments       (if solarized-midnight-brighter-comments dark-blue (doom-lighten base1 0.15)))
   (doc-comments   teal)
   (constants      magenta)
   (functions      blue)
   (keywords       green)
   (methods        cyan)
   (operators      orange)
   (type           yellow)
   (strings        cyan)
   (variables      violet)
   (numbers        magenta)
   (region         (doom-lighten bg-alt 0.15))
   (error          red)
   (warning        yellow)
   (success        green)
   (vc-modified    yellow)
   (vc-added       green)
   (vc-deleted     red)

      ;; custom categories
   (-modeline-bright solarized-midnight-brighter-modeline)
   (-modeline-pad
     (when solarized-midnight-padded-modeline
       (if (integerp solarized-midnight-padded-modeline) solarized-midnight-padded-modeline 4)))

   (modeline-fg     'unspecified)
   (modeline-fg-alt base3)

   (modeline-bg
    (if -modeline-bright
        base3
      `(,(doom-darken (car bg) 0.1) ,@(cdr base0))))
   (modeline-bg-alt
    (if -modeline-bright
        base3
      `(,(doom-darken (car bg) 0.15) ,@(cdr base0))))
   (modeline-bg-inactive     `(,(car bg-alt) ,@(cdr base1)))
   (modeline-bg-inactive-alt (doom-darken bg 0.1)))


 ;;;; Base theme face overrides
 (((font-lock-comment-face &override)
   :background (if solarized-midnight-brighter-comments (doom-lighten bg 0.05) 'unspecified))
  ((font-lock-keyword-face &override) :weight 'bold)
  ((font-lock-constant-face &override) :weight 'bold)
  ((line-number &override) :foreground base4)
  ((line-number-current-line &override) :foreground fg)
  (mode-line
   :background modeline-bg :foreground modeline-fg
   :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg)))
  (mode-line-inactive
   :background modeline-bg-inactive :foreground modeline-fg-alt
   :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive)))
  (mode-line-emphasis :foreground (if -modeline-bright base8 highlight))
  (menu :inverse-video nil :background (doom-lighten base0 0.1) :foreground base3)
  (tty-menu-enabled-face :background base0 :foreground fg)
  (tty-menu-disabled-face :background base0 :foreground base3)
  (tty-menu-selected-face :background selection :foreground base0 :weight 'bold :extend t)

  ;;;; centaur-tabs
  (centaur-tabs-active-bar-face :background blue)
  (centaur-tabs-modified-marker-selected
   :inherit 'centaur-tabs-selected :foreground blue)
  (centaur-tabs-modified-marker-unselected
   :inherit 'centaur-tabs-unselected :foreground blue)
  ;;;; company
  (company-tooltip-selection     :background dark-cyan)
  ;;;; css-mode <built-in> / scss-mode
  (css-proprietary-property :foreground orange)
  (css-property             :foreground green)
  (css-selector             :foreground blue)
  ;;;; doom-modeline
  (doom-modeline-bar :background blue)
  (doom-modeline-evil-emacs-state  :foreground base3)
  (doom-modeline-evil-insert-state :foreground green)
  ;;;; telephone-line
  (telephone-line-unimportant :background (doom-darken dark-cyan 0.2) :foreground base0 :weight 'bold)
  (telephone-line-evil-normal :background dark-blue :foreground base0 :weight 'bold)
  (telephone-line-evil-insert :background green :foreground base0 :weight 'bold)
  (telephone-line-evil-visual :background magenta :foreground base0 :weight 'bold)
  (telephone-line-evil-replace :background red :foreground base0 :weight 'bold)
  (telephone-line-evil-motion :background teal :foreground base0 :weight 'bold)
  (telephone-line-evil-operator :background orange :foreground base0 :weight 'bold)
  (telephone-line-evil-emacs :background base3 :foreground base0 :weight 'bold)
  (telephone-line-evil-god :background yellow :foreground base0 :weight 'bold)
  (telephone-line-accent-active :background (doom-darken dark-cyan 0.2) :foreground base0 :weight 'bold)
  (telephone-line-accent-inactive :background base1 :foreground base4 :weight 'bold)
  ;;;; elscreen
  (elscreen-tab-other-screen-face :background "#353a42" :foreground "#1e2022")
  ;;;; helm
  (helm-selection :inherit 'bold
                  :background selection
                  :foreground base0
                  :extend t)
  ;;;; markdown-mode
  (markdown-markup-face :foreground base5)
  (markdown-header-face :inherit 'bold :foreground red)
  (markdown-url-face    :foreground teal :weight 'normal)
  (markdown-reference-face :foreground base6)
  ((markdown-bold-face &override)   :foreground fg)
  ((markdown-italic-face &override) :foreground fg-alt)
  ((markdown-code-face &override)   :background bg-alt)
  ;;;; outline <built-in>
  ((outline-1 &override) :foreground blue)
  ((outline-2 &override) :foreground green)
  ((outline-3 &override) :foreground teal)
  ((outline-4 &override) :foreground (doom-darken blue 0.2))
  ((outline-5 &override) :foreground (doom-darken green 0.2))
  ((outline-6 &override) :foreground (doom-darken teal 0.2))
  ((outline-7 &override) :foreground (doom-darken blue 0.4))
  ((outline-8 &override) :foreground (doom-darken green 0.4))
  ;;;; org <built-in>
  ((org-block &override) :background base0)
  ((org-block-begin-line &override) :foreground comments :background base0)
  ;;;; solaire-mode
  (solaire-mode-line-face
   :inherit 'mode-line
   :background modeline-bg-alt
   :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-alt)))
  (solaire-mode-line-inactive-face
   :inherit 'mode-line-inactive
   :background modeline-bg-inactive-alt
   :box (if -modeline-pad `(:line-width ,-modeline-pad :color ,modeline-bg-inactive-alt)))))

  ;;;; Base theme variable overrides-
  ;; ()


;;; solarized-midnight-theme.el ends here
