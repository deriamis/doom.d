;;; settings/reason-mode-settings.el -*- lexical-binding: t; -*-

(add-hook! reason-mode
  (add-hook 'before-save-hook #'refmt-before-save nil t))

(provide 'reason-mode-settings)
