;;; settings/emojify-settings.el -*- lexical-binding: t; -*-

(setq emojify-display-style 'unicode)

(provide 'emojify-settings)
