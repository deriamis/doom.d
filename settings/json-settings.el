;;; settings/json-settings.el -*- lexical-binding: t; -*-

(setq json-reformat:indent-width 2)

(provide 'json-settings)
