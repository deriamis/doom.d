;;; settings/markdown-settings.el -*- lexical-binding: t; -*-

(use-package! markdown-mode
    :mode ("README\\.md\\'" . gfm-mode)
    :init
    (progn
      (setq markdown-command (executable-find "pandoc")
            markdown-asymmetric-header t
            markdown-hide-markup t
            markdown-enable-math t
            markdown-fontify-code-blocks-natively t
            markdown-gfm-uppercase-checkbox t
            markdown-enable-highlighting-syntax t)
      (defun my-gfm-mode-hook ()
        (visual-line-mode 1))
      (add-hook 'gfm-mode-hook 'my-gfm-mode-hook)))

(provide 'markdown-settings)
