;;; settings/flycheck-settings.el -*- lexical-binding: t; -*-

;; Make checking elisp files work with installed packages
(setq-default flycheck-emacs-lisp-load-path 'inherit)

;; Set up automated Python checkers
(with-eval-after-load 'python-mode
  (add-hook 'flycheck-mode-hook #'flycheck-pycheckers-setup))

;; Allow Flycheck to use Cargo project settings for checks
(with-eval-after-load 'rust-mode
  (add-hook 'flycheck-mode-hook #'flycheck-rust-setup))

;; Check for Bashisms in POSIX shell scripts
;; Check 'echo -n' usage
(setq flycheck-checkbashisms-newline t)
;; Check non-POSIX issues but required to be supported  by Debian Policy 10.4
;; Setting this variable to non nil made flycheck-checkbashisms-newline effects
;; regardless of its value
(setq flycheck-checkbashisms-posix t)
(with-eval-after-load 'shell-script-mode
  (add-hook 'flycheck-mode-hook #'flycheck-bashisms-setup))

;; Display Flycheck messages for the current line after a brief delay
(with-eval-after-load 'flycheck
  (add-hook 'flycheck-mode-hook #'flycheck-inline-mode))

;; Command to show Flycheck messages in the buffer immediately using quick-peek
(setq flycheck-inline-display-function
      (lambda (msg pos err)
        (let* ((ov (quick-peek-overlay-ensure-at pos))
               (contents (quick-peek-overlay-contents ov)))
          (setf (quick-peek-overlay-contents ov)
                (concat contents (when contents "\n") msg))
          (quick-peek-update ov)))
      flycheck-inline-clear-function #'quick-peek-hide)

(provide 'flycheck-settings)
