;;; settings/lsp-settings.el -*- lexical-binding: t; -*-

;; Set up clangd arguments
(setq lsp-clients-clangd-args '("-j=3"
                                "--background-index"
                                "--clang-tidy"
                                "--completion-style=detailed"
                                "--header-insertion=never"
                                "--header-insertion-decorators=0"))

;; Make clangd the default for C and C++ files
(after! lsp-clangd (set-lsp-priority! 'clangd 2))
(set-eglot-client! 'cc-mode '("clangd" "-j=3" "--clang-tidy"))

(provide 'lsp-settings)
