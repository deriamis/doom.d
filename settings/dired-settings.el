;;; settings/dired-settings.el -*- lexical-binding: t; -*-

(setq dired-dwim-target t)

(provide 'dired-settings)
