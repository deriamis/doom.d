;;; settings/menu-bar-settings.el -*- lexical-binding: t; -*-

(menu-bar-mode t)

(provide 'menu-bar-settings)
