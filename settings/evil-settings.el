;;; settings/evil-settings.el -*- lexical-binding: t; -*-

;; The scope of evil-snipe should be the entire visible buffer.
(setq evil-snipe-scope 'whole-visible)

(provide 'evil-settings)
