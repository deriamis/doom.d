;;; settings/mac.el -*- lexical-binding: t; -*-

(cond (IS-MAC
       (setq mac-command-modifier       'meta
             mac-option-modifier        'alt
             mac-right-option-modifier  'alt
             mac-pass-control-to-system nil)))

(provide 'mac-settings)
