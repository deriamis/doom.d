;;; settings/auctex-settings.el -*- lexical-binding: t; -*-

(add-hook! LaTeX-mode (setq-local display-line-numbers nil))

(provide 'auctex-settings)
