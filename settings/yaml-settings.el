;;; settings/yaml-settings.el -*- lexical-binding: t; -*-

(add-to-list 'hs-special-modes-alist '(yaml-mode "\\s-*\\_<\\(?:[^:]+\\)\\_>" "" "#" +data-hideshow-forward-sexp nil))

(provide 'yaml-settings)
