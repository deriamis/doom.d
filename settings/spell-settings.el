;;; settings/spell-settings.el -*- lexical-binding: t; -*-

(after! flyspell
  ;; Select default dictionary.
  (setq ispell-dictionary "en_US")
  (ispell-change-dictionary "en_US")

  ;; Function to switch dictionaries.
  (defun deriamis/flyspell-switch-dictionary ()
    (interactive)
    (let* ((old_dic ispell-current-dictionary)
           (new_dic (if (string= old_dic "en_US") "en_US")))
      (ispell-change-dictionary new_dic)
      (message "Dictionary switched to %s." new_dic))))

(provide 'spell-settings)
