;;; settings/javascript-settings.el -*- lexical-binding: t; -*-

(setq js-indent-level 2
      typescript-indent-level 2
      prettier-js-args '("--single-quote"))

(add-hook!
  js2-mode 'prettier-js-mode
  (add-hook 'before-save-hook #'refmt-before-save nil t))

(provide 'javascript-settings)
