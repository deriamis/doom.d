;;; settings/web-mode-settings.el -*- lexical-binding: t; -*-

(setq  web-mode-markup-indent-offset 2
       web-mode-code-indent-offset 2
       web-mode-css-indent-offset 2
       css-indent-offset 2)

(after! web-mode
  (add-to-list 'auto-mode-alist '("\\.njk\\'" . web-mode)))

(provide 'web-mode-settings)
