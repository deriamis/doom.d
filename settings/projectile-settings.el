;;; settings/projectile-settings.el -*- lexical-binding: t; -*-

(setq projectile-project-search-path '("~/Projects" "~/Workspace"))

(provide 'projectile-settings)
