;;; settings/editor-settings.el -*- lexical-binding: t; -*-

(setq kill-whole-line t)

(setq auto-save-default t
      make-backup-files t)

(global-auto-revert-mode t)

(use-package! visual-regexp-steroids
  :defer 3
  :config
  (use-package! pcre2el
    :config
    (setq vr/engine 'pcre2el)
    (map! "C-c s r" #'vr/replace)
    (map! "C-c s q" #'vr/query-replace)))

(defun +data-hideshow-forward-sexp (arg)
  (let ((start (current-indentation)))
    (forward-line)
    (unless (= start (current-indentation))
      (require 'evil-indent-plus
       (let ((range (evil-indent-plus--same-indent-range)))
         (goto-char (cadr range))
         (end-of-line))))))

(defmacro disallow-cd-in-function (fun)
  "Prevent FUN (or any function that FUN calls) from changing directory."
  `(defadvice ,fun (around dissallow-cd activate)
     (let ((old-dir default-directory) ; Save old directory
           (new-buf ad-do-it)) ; Capture new buffer
       ;; If FUN returns a buffer, operate in that buffer in addition
       ;; to current one.
       (when (bufferp new-buf)
         (set-buffer new-buf)
         (setq default-directory old-dir))
       ;; Set default-directory in the current buffer
       (setq default-directory old-dir))))

(disallow-cd-in-function find-file-noselect-1)
(disallow-cd-in-function set-visited-file-name)

(provide 'editor-settings)
