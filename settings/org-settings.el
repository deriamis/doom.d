;;; settings/org-settings.el -*- lexical-binding: t; -*-

;; =============================================================================
;;                            Org files definition
;; =============================================================================

;; Events directory.
(defvar deriamis/+org-events-directory
  "Events")

;; Journal directory.
(defvar deriamis/+org-journal-directory
  "Journal")

;; TODO file for org-capture.
(defvar deriamis/+org-capture-inbox-file
  "deriamis.net Inbox")

;; Wiki directory.
(defvar deriamis/+org-wiki-directory
  "Wiki")

(setq deriamis/org-events-directory
      (expand-file-name deriamis/+org-events-directory org-directory))
(setq deriamis/org-capture-inbox-file
      (expand-file-name deriamis/+org-capture-inbox-file deriamis/+org-events-directory))
(setq deriamis/org-journal-directory
      (expand-file-name deriamis/+org-journal-directory org-directory))
(setq deriamis/org-wiki-directory
      (expand-file-name deriamis/+org-wiki-directory org-directory))

;; =============================================================================
;;                                    Org
;; =============================================================================

(after! org
  (progn
    (setq org-bullets-bullet-list '("·")
          org-ellipsis " ▾ "
          org-fancy-priorities-list '("⚡" "⬆" "⬇" "☕")
          org-log-done 'time
          org-refile-targets (quote ((nil :maxlevel . 1)))
          org-startup-folded 'content
          org-tags-column +81)
    (add-hook! 'org-mode-hook #'+org-pretty-mode #'mixed-pitch-mode)
    (add-hook! 'org-mode-hook #'solaire-mode)
    (add-hook! 'org-mode-hook (company-mode -1))
    (add-hook! 'org-capture-mode-hook (company-mode -1))
    (remove-hook! 'doom-first-file-hook 'save-place-mode)))

(set-popup-rule! "^\\*Org Agenda" :side 'bottom :size 0.90 :select t :ttl nil)
(set-popup-rule! "^CAPTURE.*\\.org$" :side 'bottom :size 0.90 :select t :ttl nil)
(set-popup-rule! "^\\*org-brain" :side 'right :size 1.00 :select t :ttl nil)

;; =============================================================================
;;                                 Org agenda
;; =============================================================================

(setq org-agenda-files deriamis/org-events-directory
      org-super-agenda-groups
        '(
          (:name "Today"
           :time-grid t
           :scheduled today)
          (:name "Due today"
                 :deadline today)
          (:name "Important"
                 :priority "A")
          (:name "Overdue"
                 :deadline past)
          (:name "Due soon"
                 :deadline future)
          (:name "Big Outcomes"
                 :tag "bo")))
(after! org
  (setq-default
   org-agenda-window-setup 'current-window
   org-agenda-skip-unavailable-files t
   org-agenda-start-day "-3d"
   org-agenda-span 10
   org-agenda-repeating-timestamp-show-all nil
   org-agenda-remove-tags t
   org-agenda-prefix-format " %-11.11c %?-12t% s"
   org-agenda-todo-keyword-format "✓️"
   org-agenda-scheduled-leaders '("Scheduled  " "Sched.%2dx  ")
   org-agenda-deadline-leaders '("Deadline   " "In %3d d.  " "%2d d. ago  ")
   org-agenda-time-grid '((daily today remove-match)
                          (0800 1000 1200 1400 1600 1800 2000 2200)
                          "      " "-----------")
   org-agenda-current-time-string "◀ ----- now"))

;; =============================================================================
;;                                Org capture
;; =============================================================================

(after! org
  (after! all-the-icons
    (setq +org-capture-todo-file deriamis/org-events-directory
          org-capture-templates
          (doct '(("%{name}"
                   :name (lambda () (format "%s\tNote" (all-the-icons-faicon "sticky-note" :face 'all-the-icons-green :v-adjust 0.01)))
                   :keys "n"
                   :file deriamis/+org-capture-inbox-file
                   :headline "Notes"
                   :hook (lambda () (ispell-change-dictionary "en_US"))
                   :prepend t
                   :kill-buffer t
                   :type entry
                   :template ("* [ ] %U %?"
                              "%i %a"))
                  ("%{name}"
                   :name (lambda () (format "%s\tEvent" (all-the-icons-octicon "calendar" :face 'all-the-icons-yellow :v-adjust 0.01)))
                   :keys "s"
                   :file deriamis/+org-capture-inbox-file
                   :headline "Events"
                   :hook (lambda () (ispell-change-dictionary "en_US"))
                   :prepend t
                   :kill-buffer t
                   :type entry
                   :template ("* [ ] %?\nSCHEDULED: %^{Start:}t"
                              "%i"))
                  ("%{name}"
                   :name (lambda () (format "%s\tTask" (all-the-icons-octicon "inbox" :face 'all-the-icons-blue :v-adjust 0.01)))
                   :keys "a"
                   :file deriamis/+org-capture-inbox-file
                   :headline "Tasks"
                   :hook (lambda () (ispell-change-dictionary "en_US"))
                   :prepend t
                   :kill-buffer t
                   :type entry
                   :template ("* [ ] TODO %?%{extra}"
                              "%i")
                   :children (("%{name}"
                               :name (lambda () (format "%s\tNo deadline" (all-the-icons-octicon "checklist" :face 'all-the-icons-green :v-adjust 0.01)))
                               :keys "g"
                               :extra "")
                              ("%{name}"
                               :name (lambda () (format "%s\tHas deadline" (all-the-icons-material "timer" :face 'all-the-icons-yellow :v-adjust -0.1)))
                               :keys "p"
                               :extra "\nDEADLINE: %^{Deadline:}t")
                              ("%{name}"
                               :name (lambda () (format "%s\tHas appointment"(all-the-icons-octicon "calendar" :face 'all-the-icons-blue :v-adjust 0.01)))
                               :keys "a"
                               :extra "\nSCHEDULED: %^{Start:}t"))))))))

;; =============================================================================
;;                                 Org journal
;; =============================================================================

(use-package! org-journal
  :after org
  :config
  ;; Journal directory.
  (setq org-journal-dir deriamis/org-journal-directory)

  ;; Record journals monthly.
  (setq org-journal-file-type 'monthly)

  ;; Journal file format.
  (setq org-journal-file-format "%Y/Journal-%m.org")

  ;; Week starts on Sunday.
  (setq org-journal-start-on-weekday 0)

  ;; Set date format.
  (setq org-journal-date-format "%A, %B %e, %Y (%d/%m/%Y)")

  ;; Header of the journal file.
  (setq org-journal-file-header
        "#+TITLE: Journal for %B %Y.\n\
#+AUTHOR: Ryan Egesdahl\n\
#+STARTUP: content\n")

  ;; Hook after an entry is created.
  (add-hook 'org-journal-after-entry-create-hook
            (lambda ()
              ;; Always start in insert mode.
              (evil-insert-state)
              ;; The spelling language should be pt_BR.
              (ispell-change-dictionary "en_US"))))

(provide 'org-settings)
