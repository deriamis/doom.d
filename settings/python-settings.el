;;; settings/python-settings.el -*- lexical-binding: t; -*-

(use-package! auto-virtualenv
  :init
  (use-package! pyvenv
    :config
    (add-hook 'python-mode-hook 'auto-virtualenv-set-virtualenv)
    (add-hook 'projectile-after-switch-project-hook 'auto-virtualenv-set-virtualenv)))

(provide 'python-settings)
