;;; settings/uncrustify-settings.el -*- lexical-binding: t; -*-

(use-package! uncrustify-mode
  :init
  (add-hook 'c-mode-common-hook
            #'(lambda ()
                uncrustify-mode 1)))

(provide 'uncrustify-settings)
