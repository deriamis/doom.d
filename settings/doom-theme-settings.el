;;; settings/doom-theme-settings.el -*- lexical-binding: t; -*-

;; (doom-themes-visual-bell-config)
(doom-themes-treemacs-config)
(doom-themes-org-config)

(provide 'doom-theme-settings)
