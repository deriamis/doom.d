;;; settings/ui-settings.el -*- lexical-binding: t; -*-

;; Set the graphical splash
(let ((alternatives '("doom-emacs-bw-light.svg"
                      "doom-emacs-color2.png"
                      "doom-emacs-slant-out-bw.png")))
  (setq fancy-splash-image
        (concat doom-private-dir "splash/"
                (nth (random (length alternatives)) alternatives))))

;; Edit the available options on the dashboard
(setq +doom-dashboard-menu-sections (cl-subseq +doom-dashboard-menu-sections 0 5))

;; Ensure any non-Doom themes inherits the variable-pitch face for the modeline
(custom-set-faces! '(mode-line-active :inherit mode-line))

;; disable global-hl-line
;; oddly, that's the way: https://github.com/hlissner/doom-emacs/issues/4206
(remove-hook 'doom-first-buffer-hook #'global-hl-line-mode)

;; Make the titlebar transparent
(add-to-list 'default-frame-alist
             '(ns-transparent-titlebar . t))

;; Default new frames to dark mode
(add-to-list 'default-frame-alist
             '(ns-appearance . dark))

;; Don't annoy me with prompts!
(setq use-short-answers t
      confirm-nonexistent-file-or-buffer nil
      confirm-kill-emacs nil
      inhibit-startup-message t
      inhibit-startup-echo-area-message t
      kill-buffer-query-functions
  (remq 'process-kill-buffer-query-function
         kill-buffer-query-functions))

(provide 'ui-settings)
