;;; settings/magit-settings.el -*- lexical-binding: t; -*-

(setq +magit-hub-features t)

(provide 'magit-settings)
