;;; settings/mode-icons-settings.el -*- lexical-binding: t; -*-

(use-package! mode-icons
  :hook (after-init . mode-icons-mode)
  :config
  (setq mode-icons-change-mode-name nil
        mode-icons-desaturate-active t))

(provide 'mode-icons-settings)
