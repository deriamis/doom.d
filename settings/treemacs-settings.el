;;; settings/treemacs-settings.el -*- lexical-binding: t; -*-

(setq treemacs-follow-after-init t
      treemacs-use-follow-mode t)

(provide 'treemacs-settings)
