;;; settings/keybindings.el -*- lexical-binding: t; -*-

;; Configure global keybindings.

(map! "<f5>" #'neotree-toggle
      "<f6>" #'deriamis/flyspell-switch-dictionary)

(map! :leader
      "<down>"  #'windmove-down
      "<left>"  #'windmove-left
      "<right>" #'windmove-right
      "<up>"    #'windmove-up)

(map! :leader
      (:prefix "c"
       (:prefix-map ("c" . "comment")
        :desc "Right align comment"  "r" #'deriamis/comment-right-align
        :desc "Center comment"       "c" #'deriamis/comment-center-align))
      (:prefix-map ("t" . "text")
       (:prefix ("a" . "align")
        :desc "Right align text"  "r" #'deriamis/text-right-align
        :desc "Center text"       "c" #'deriamis/text-center-align)
       (:prefix ("f" . "fill")
        :desc "Copy character"   "c" #'deriamis/copy-to-end
        :desc "Fill with char."  "f" #'deriamis/fill-to-end
        :desc "Fill entire line" "l" #'deriamis/fill-line))
      (:prefix "o"
       :desc "Org directory" "D" (lambda () (interactive) (dired deriamis/org-directory))))

(map! :nvi "<home>" #'evil-beginning-of-line
      :nvi "<end>" #'evil-end-of-line
      :nv "<down>" #'evil-next-visual-line
      :nv "<up>"   #'evil-previous-visual-line)

(map! :ne "M-/" #'comment-or-uncomment-region)
(map! :ne "SPC / r" #'deadgrep)
(map! :ne "SPC n b" #'org-brain-visualize)

(provide 'keybindings)
