;;; settings/auth-settings.el -*- lexical-binding: t; -*-

(after! auth-source
  (setq auth-sources (nreverse auth-sources)))

(provide 'auth-settings)
