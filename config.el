;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-

;;(when init-file-debug
;;  (require 'benchmark-init)
;;  (add-hook 'doom-first-input-hook #'benchmark-init/deactivate))

;; Load paths for local settings and packages.
(let ((default-directory (expand-file-name "packages" doom-user-dir)))
  (normal-top-level-add-subdirs-to-load-path))
(add-load-path! "settings")
(add-load-path! "lisp")

;; Directory to start in when none is provided
(setq default-directory "~")

;; Set the base directory for Org
(setq org-directory "~/Documents/org")

;; Some functionality uses this to identify you, e.g. GPG configuration, email
;; clients, file templates and snippets. It is optional.
(setq user-full-name "Ryan Egesdahl"
      user-mail-address "regesdahl@gitlab.com")

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-symbol-font' -- for symbols
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; See 'C-h v doom-font' for documentation and more examples of what they
;; accept. For example:
;;
(setq doom-font (font-spec :family "Iosevka Fixed" :size 16 :weight 'light)
      doom-big-font (font-spec :family "Iosevka Fixed" :size 24)
      doom-variable-pitch-font (font-spec :family "Iosevka" :size 16))

;; Load themes - the modeline theme must come before the main theme
(setq doom-theme 'solarized-midnight)

;; Select locale.
(setenv "LANG" "en_US.UTF-8")
(setenv "LC_CTYPE" "en_US.UTF-8")
(setenv "LC_ALL" "en_US.UTF-8")
(set-locale-environment "en_US.UTF-8")

;; Local packages.
(require 'telephone-line)

;; Local functions.
(require 'comment-align)
(require 'fill-line)
(require 'text-align)

;; Custom settings
(require 'keybindings)
(require 'auctex-settings)
(require 'auth-settings)
(require 'doom-theme-settings)
(require 'editor-settings)
(require 'emojify-settings)
(require 'evil-settings)
(require 'flycheck-settings)
(require 'javascript-settings)
(require 'json-settings)
(require 'lsp-settings)
(require 'mac-settings)
(require 'magit-settings)
(require 'markdown-settings)
(require 'menu-bar-settings)
(require 'mode-icons-settings)
(require 'org-settings)
(require 'prog-settings)
(require 'projectile-settings)
(require 'python-settings)
(require 'reason-mode-settings)
(require 'ruby-settings)
(require 'spell-settings)
(require 'telephone-line-settings)
(require 'treemacs-settings)
(require 'ui-settings)
(require 'uncrustify-settings)
(require 'vterm-settings)
(require 'web-mode-settings)
(require 'yaml-settings)
